'use strict'

let path = require('path')
let express = require('express')
let webpack = require('webpack')
let serveStatic = require('serve-static')

let config = require('./webpack.client.config')

let app = express()
let compiler = webpack(config)

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

app.use(serveStatic('./public/'))

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/index.html'))
})

app.listen(3000, 'localhost', function (err) {
  if (err) {
    console.error(err)
    return 0
  }

  console.info('Ready at http://localhost:3000')
})
