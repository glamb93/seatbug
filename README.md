[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

# SeatBug

Find your a new mate !

## Main Technologies

* React
* Redux
* NodeJS
* And smaller libs

## NPM Scripts

May have trouble with the npm scripts on Windows, caused by the environment variable set in the
commands.

## Data

### Airport

data.json coming from either:

* http://data.okfn.org/data/core/airport-codes
* https://gist.githubusercontent.com/tdreyno/4278655/raw/7b0762c09b519f40397e4c3e100b097d861f5588/airports.json
* https://github.com/mwgg/Airports/blob/master/airports.json
* https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat (currently using this one)

## Conventions

### In Javascript

##### Babel

* Please use the wonderful features of the divine ES7. All thanks to babel :)

##### Code Style

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

##### Import

* Usually separated in 4 blocks (in this order):

```javascript
import a from 'lib-for-test'

import b from 'lib-for-app'

import c from 'own-module-for-test'

import d from 'own-module-for-app'
```

* Unless in same directory or below in the tree, use path relative to project root
* Use '~/' to write an import path relative to the project root
* Here '~' is converted into the absolute project path during during babel transpiling
* No '..' in the import path
* Use relative path for export of this type: `export foo from '../foo'` (not supported by the plugin
  yet)
* See babel-root-import plugin for more info

##### Function

* Always use `function` instead of the arrow functions for more than one liners.
* Use arrow functions for one-line function
* Arrow functions are also used to lexically bind this.

##### Containers vs. High Order Component

* Containers don't have presentation content. They only have callbacks, mapStateToProps, etc.
* High-order components don't have logic(what do the callbacks do or how to map the state/action to
  the props) but only presentation

##### Reducer / actions organization

Inspired by ducks here https://github.com/erikras/ducks-modular-redux
Few changes:
* Ducks may contain more than one module
* Ducks have a file, reducer.js, that export the reducer/combined-reducer
* Ducks have a file, action.js, that export the actions public actions (internal actions are exposed only
  in their respective modules for testing)
* Reducer initialize ALL elements of the state to avoid having to test if they exist all the time
* Reducer use a deep copy of the INITIAL_STATE constant so we can use it for setting up tests

##### Unit Test

* Use `shallowRender` when no need of refs
* Use `render` otherwise
