'use strict'

const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')

const ENV = process.env.NODE_ENV

const definePlugin = new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify(JSON.parse(ENV === 'production' ? '"production"' : '"dev"'))
})

const common = {
  entry: [
    './src/client/index'
  ],
  output: {
    path: path.join(__dirname, '/public'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    definePlugin
  ],
  module: {
    loaders: [{
      test: /\.js?$/,
      exclude: /(node_modules|public)/,
      loader: 'babel-loader'
    }]
  }
}

// Default configuration
if (ENV === 'production' || !ENV) {
  module.exports = merge(common, {})
} else {
  module.exports = merge(common, {
    devtool: 'cheap-module-eval-source-map',
    entry: [
      'webpack-hot-middleware/client'
    ],
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin()
    ]
  })
}
