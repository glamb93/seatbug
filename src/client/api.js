'use strict'

import fetch from 'isomorphic-fetch'

let apiInstance

export default function getInstance () {
  return apiInstance
}

export function initializeApi (apiUrl) {
  apiInstance = Api(apiUrl)
}

export function setApi (api) {
  apiInstance = api
}

function Api (apiUrl) {
  async function post (relativeUrl, body) {
    try {
      const res = await fetch(apiUrl + '/' + withoutFirstSlash(relativeUrl), {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
      })

      if (res.status >= 400) {
        throw new ServerError(res.status)
      }

      return await res.json()
    } catch (err) {
      throw new NetworkError(err)
    }
  }

  function withoutFirstSlash (relativeUrl) {
    if (relativeUrl.slice(0, 1) === '/') {
      return relativeUrl.slice(1)
    } else {
      return relativeUrl
    }
  }

  return {
    post
  }
}

export class ServerError {
  constructor (message) {
    this.message = message
  }
}

export class NetworkError {
  constructor (message) {
    this.message = message
  }
}
