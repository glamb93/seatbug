'use strict'

import airportSearch from '~/client/ducks/airports/reducer.js'

export default {
  airportSearch
}
