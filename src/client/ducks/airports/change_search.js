'use strict'

import getApi from '~/client/api'

const CHANGE_SEARCH = 'search/CHANGE_SEARCH'
const MATCH_SUGGESTIONS = 'search/MATCH_SUGGESTIONS'
const HIGHLIGHT_NEXT = 'search/HIGHLIGHT_NEXT'
const HIGHLIGHT_PREVIOUS = 'search/HIGHLIGHT_PREVIOUS'
const HIGHLIGHT_SUGGESTION = 'search/HIGHLIGHT_SUGGESTION'
const BLUR_SEARCH = 'search/HIDE'
const FOCUS_SEARCH = 'search/FOCUS'

const INITIAL_STATE = {
  suggestions: [],
  isWaitingForFirstSuggestions: true,
  currentSearch: '',
  highlightedSuggestion: 0,
  hasFocus: false
}

export default function reducer (state = { ...INITIAL_STATE }, action) {
  switch (action.type) {
    case CHANGE_SEARCH:
      const conditionalChange = {}
      if (!action.searchText) {
        conditionalChange.isWaitingForFirstSuggestions = true
      }

      return {
        ...state,
        ...conditionalChange,
        currentSearch: action.searchText
      }

    case MATCH_SUGGESTIONS:
      if (action.searchText === state.currentSearch) {
        return {
          ...state,
          highlightedSuggestion: 0,
          isWaitingForFirstSuggestions: false,
          suggestions: action.matchingAirports
        }
      } else {
        return state
      }

    case HIGHLIGHT_NEXT:
      if (state.suggestions.length !== 0) {
        return {
          ...state,
          highlightedSuggestion: (state.highlightedSuggestion + 1) % state.suggestions.length
        }
      } else {
        return state
      }

    case HIGHLIGHT_PREVIOUS:
      const nSuggestions = state.suggestions.length
      if (nSuggestions !== 0) {
        return {
          ...state,
          highlightedSuggestion: (state.highlightedSuggestion - 1 + nSuggestions) % nSuggestions
        }
      } else {
        return state
      }

    case HIGHLIGHT_SUGGESTION:
      const highlightedSuggestion = state.suggestions.findIndex((s) => s.code === action.airportCode)
      return {
        ...state,
        highlightedSuggestion
      }

    case BLUR_SEARCH:
      return {
        ...state,
        hasFocus: false
      }

    case FOCUS_SEARCH:
      return {
        ...state,
        hasFocus: true
      }

    default:
      return state
  }
}

export function airportChangeSearch (searchText) {
  return async function (dispatch) {
    dispatch(changeCurrentSearch(searchText))
    try {
      const res = await getApi().post('/airports', { search: searchText })
      const matchingAirports = res.airports
      dispatch(matchSuggestions(searchText, matchingAirports))
    } catch (err) {
      console.error(err)
    }
  }
}

export function changeCurrentSearch (searchText) {
  return {
    type: CHANGE_SEARCH,
    searchText
  }
}

export function matchSuggestions (searchText, matchingAirports) {
  return {
    type: MATCH_SUGGESTIONS,
    searchText,
    matchingAirports
  }
}

export function highlightNext () {
  return {
    type: HIGHLIGHT_NEXT
  }
}

export function highlightPrevious () {
  return {
    type: HIGHLIGHT_PREVIOUS
  }
}

export function blurSearch () {
  return {
    type: BLUR_SEARCH
  }
}

export function focusSearch () {
  return {
    type: FOCUS_SEARCH
  }
}

export function highlightSuggestion (airportCode) {
  return {
    type: HIGHLIGHT_SUGGESTION,
    airportCode
  }
}
