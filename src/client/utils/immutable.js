'use strict'

export function remove (array, selectorFn) {
  const element = array.find(selectorFn)
  if (element === undefined) {
    return array
  }

  const index = array.indexOf(element)
  const before = array.slice(0, index)
  const after = array.slice(index + 1, array.length)

  return before.concat(after)
}
