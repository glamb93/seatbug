'use strict'

import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import SearchBarComponent from '~/client/components/high_orders/search_bar_component'
import {
  airportChangeSearch,
  highlightNext,
  highlightPrevious,
  highlightSuggestion,
  blurSearch,
  focusSearch
} from '~/client/ducks/airports/actions'

export class SearchBar extends React.Component {
  render () {
    const {
      airportChangeSearch,
      highlightNext,
      highlightPrevious,
      highlightSuggestion,
      blurSearch,
      focusSearch,

      isShowingSuggestions,
      suggestions,
      highlightedSuggestion
    } = this.props

    return (
      <SearchBarComponent
        handleChange={airportChangeSearch}
        handleBlurSearch={blurSearch}
        handleFocusSearch={focusSearch}
        goDownSuggestions={highlightNext}
        goUpSuggestions={highlightPrevious}
        highlightSuggestion={highlightSuggestion}

        isShowingSuggestions={isShowingSuggestions}
        suggestions={suggestions}
        highlightedSuggestion={highlightedSuggestion}
      />
    )
  }
}

SearchBar.propTypes = {
  airportChangeSearch: React.PropTypes.func.isRequired,
  highlightNext: React.PropTypes.func.isRequired,
  highlightPrevious: React.PropTypes.func.isRequired,
  highlightSuggestion: React.PropTypes.func.isRequired,
  blurSearch: React.PropTypes.func.isRequired,
  focusSearch: React.PropTypes.func.isRequired,

  isShowingSuggestions: React.PropTypes.bool.isRequired,
  suggestions: React.PropTypes.array.isRequired,
  highlightedSuggestion: React.PropTypes.number.isRequired
}

export function mapStateToProps (state) {
  const subState = state.airportSearch

  const _isShowingSuggestions = isShowingSuggestions(
    subState.hasFocus,
    subState.currentSearch,
    subState.suggestions,
    subState.isWaitingForFirstSuggestions
  )

  return {
    isShowingSuggestions: _isShowingSuggestions,
    suggestions: subState.suggestions,
    highlightedSuggestion: subState.highlightedSuggestion
  }
}

function isShowingSuggestions (hasFocus, currentSearch, suggestions, isWaitingForFirstSuggestions) {
  if (hasFocus && currentSearch) {
    if (suggestions.length !== 0 || !isWaitingForFirstSuggestions) {
      return true
    }
  }

  return false
}

function mapDispatchToProps (dispatch) {
  return {
    airportChangeSearch: bindActionCreators(airportChangeSearch, dispatch),
    highlightNext: bindActionCreators(highlightNext, dispatch),
    highlightPrevious: bindActionCreators(highlightPrevious, dispatch),
    highlightSuggestion: bindActionCreators(highlightSuggestion, dispatch),
    blurSearch: bindActionCreators(blurSearch, dispatch),
    focusSearch: bindActionCreators(focusSearch, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)
