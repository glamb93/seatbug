'use strict'

import React from 'react'
import { Provider } from 'react-redux'
import { Router, hashHistory } from 'react-router'
import {
  syncHistoryWithStore
} from 'react-router-redux'

import { routes } from '~/client/routes'
import * as Store from '~/client/store'

const history = hashHistory

Store.setHistory(history)

export default class Root extends React.Component {
  componentWillMount () {
    this.store = Store.getStore()
    this.syncedHistory = syncHistoryWithStore(history, this.store)
  }

  render () {
    return (
      <Provider store={this.store}>
        <Router routes={routes} history={this.syncedHistory} />
      </Provider>
    )
  }
}
