'use strict'

import React from 'react'

import SearchBar from '~/client/containers/search_bar'

export default class App extends React.Component {
  render () {
    return (
      <div>
        <SearchBar />
        {this.props.children}
      </div>
    )
  }
}
