'use strict'

import React from 'react'
import { Route } from 'react-router'

import App from '~/client/containers/app'
import Airport from '~/client/containers/airport'

export const ROUTES = {
  home: '/',
  airport: '/airport'
}

export const routes = (
  <Route path={ROUTES.home} component={App}>
    <Route path={ROUTES.airport} component={Airport} />
  </Route>
)
