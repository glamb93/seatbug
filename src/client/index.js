'use strict'

require('babel-polyfill')

import React from 'react'
import { render } from 'react-dom'

import Root from './containers/root'
import { initializeApi } from './api'

// The process.env.NODE_ENV is preprocessed by webpack with the define plugin
const API_URL = process.env.NODE_ENV === 'production' ? 'http://seatbug1-simplicity.rhcloud.com' : 'http://localhost:3031'

initializeApi(API_URL)

render(
  <Root />,
  document.getElementById('root')
)
