'use strict'

import Radium from 'radium'
import React from 'react'

import Suggestions from '~/client/components/suggestions'

const UP_ARROW_CODE = 38
const DOWN_ARROW_CODE = 40

class SearchBarComponent extends React.Component {
  constructor () {
    super()
    this.handleKeyDown = this.handleKeyDown.bind(this)
    this.renderSuggestionsIfNecessary = this.renderSuggestionsIfNecessary.bind(this)
  }

  handleKeyDown (event) {
    switch (event.keyCode) {
      case DOWN_ARROW_CODE:
        this.props.goDownSuggestions()
        break

      case UP_ARROW_CODE:
        this.props.goUpSuggestions()
        break
    }
  }

  renderSuggestionsIfNecessary () {
    const {
      highlightSuggestion,
      highlightedSuggestion,
      isShowingSuggestions,
      suggestions
    } = this.props

    if (isShowingSuggestions) {
      return <Suggestions
        highlightSuggestion={highlightSuggestion}

        suggestions={suggestions}
        highlightedSuggestion={highlightedSuggestion}
      />
    }

    return undefined
  }

  render () {
    const handleChange = (event) => this.props.handleChange(event.target.value)

    const {
      handleBlurSearch,
      handleFocusSearch
    } = this.props

    return (
      <div style={styles.searchBar} >
        <form>
          <input
            ref='input'
            onKeyDown={this.handleKeyDown}
            onChange={handleChange}
            onBlur={handleBlurSearch}
            onFocus={handleFocusSearch}
            autoFocus
            placeholder='Where are you going, my friend ?'
            style={styles.input}
          />
        </form>
        {this.renderSuggestionsIfNecessary()}
      </div>
    )
  }
}

const styles = {
  searchBar: {
    width: '30em',
    marginTop: '5em',
    padding: '2em',
    boxShadow: '10px 10px 5px #888888',
    backgroundColor: '#ffffA0'
  },
  input: {
    boxSizing: 'border-box',
    width: '100%',
    padding: '0.6em 1em',
    fontSize: '150%',
    ':focus': {
      outline: '0.1em solid #eec522'
    }
  }
}

SearchBarComponent.propTypes = {
  handleChange: React.PropTypes.func.isRequired,
  handleBlurSearch: React.PropTypes.func.isRequired,
  handleFocusSearch: React.PropTypes.func.isRequired,
  goDownSuggestions: React.PropTypes.func.isRequired,
  goUpSuggestions: React.PropTypes.func.isRequired,

  isShowingSuggestions: React.PropTypes.bool.isRequired,
  highlightedSuggestion: React.PropTypes.number.isRequired,
  suggestions: React.PropTypes.array.isRequired
}

export default Radium(SearchBarComponent)
