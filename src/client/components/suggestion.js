'use strict'

import Radium from 'radium'
import React from 'react'

const suggestionsHeight = '3.5em'

class Suggestion extends React.Component {
  render () {
    const {
      highlightSuggestion,
      airport,
      isHighlighted
    } = this. props

    const a = airport
    const onMouseEnter = () => highlightSuggestion(a.code)

    return (
      <li key={a.code} onMouseEnter={onMouseEnter} style={
        [
          styles.suggestion,
          isHighlighted && styles.highlighted
        ]}>
        <div style={styles.codeWrapper}>
          <p style={styles.code}>{a.code}</p>
        </div>
        <div style={styles.rightSideWrapper}>
          <p style={styles.name} >{a.name}</p>
          <p style={styles.location} >{a.city} - {a.country}</p>
        </div>
      </li>
    )
  }
}

Suggestion.propTypes = {
  highlightSuggestion: React.PropTypes.func.isRequired,
  airport: React.PropTypes.object.isRequired,
  isHighlighted: React.PropTypes.bool.isRequired
}

export default Radium(Suggestion)

const styles = {
  suggestion: {
    boxSizing: 'border-box',
    height: suggestionsHeight,
    width: '100%',
    padding: '0 2em',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    listStyleType: 'none'
  },
  codeWrapper: {
    display: 'inline-flex',
    position: 'relative',
    height: suggestionsHeight,
    width: suggestionsHeight
  },
  code: {
    display: 'flex',
    margin: 'auto',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '120%',
    fontWeight: 'bold'
  },
  rightSideWrapper: {
    display: 'inline-block',
    verticalAlign: 'middle',
    marginLeft: '1em'
  },
  name: {
    fontSize: '100%'
  },
  location: {
    fontSize: '80%'
  },
  highlighted: { backgroundColor: '#ffd633' }
}
