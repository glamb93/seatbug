'use strict'

import Radium from 'radium'
import React from 'react'

import Suggestion from '~/client/components/suggestion'

export default class Suggestions extends React.Component {
  constructor () {
    super()
    this.renderNoSuggestions = this.renderNoSuggestions.bind(this)
    this.renderSuggestions = this.renderSuggestions.bind(this)
  }

  renderNoSuggestions () {
    return <li ref='noSuggestionsElement' style={styles.noSuggestions}>No suggestions</li>
  }

  renderSuggestions () {
    const {
      highlightSuggestion,

      suggestions,
      highlightedSuggestion
    } = this.props

    return suggestions.map(function (s, i) {
      const isHighlighted = i === highlightedSuggestion

      return (
        <Suggestion
          key={s.code}
          highlightSuggestion={highlightSuggestion}
          airport={s}
          isHighlighted={isHighlighted}
          ref={isHighlighted ? 'highlightedSuggestion' : undefined}
        />
      )
    })
  }

  render () {
    const suggestions = this.props.suggestions
    return (
      <ul ref='suggestions'>
        {suggestions.length === 0 ? this.renderNoSuggestions() : this.renderSuggestions()}
      </ul>
    )
  }
}

Suggestions.propTypes = {
  highlightSuggestion: React.PropTypes.func.isRequired,
  suggestions: React.PropTypes.array.isRequired,
  highlightedSuggestion: React.PropTypes.number.isRequired
}

export default Radium(Suggestions)

const styles = {
  noSuggestions: {
    display: 'inline-flex',
    height: '3em',
    padding: '0 2em',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '140%',
    color: '#885'
  }
}
