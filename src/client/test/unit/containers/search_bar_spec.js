'use strict'

/* global describe, beforeEach, it */

import assert from 'assert'
import sinon from 'sinon'

import React from 'react'

import sd from '~/client/test/lib/skin_deep_wrapper'
import { getInitialStoreState } from '~/client/test/unit/test_helper'

import {
  SearchBar,
  mapStateToProps
} from '~/client/containers/search_bar'

describe('Search Bar Container', function () {
  const airportChangeSearch = sinon.spy()
  const highlightNext = sinon.spy()
  const highlightPrevious = sinon.spy()
  const blurSearch = sinon.spy()
  const focusSearch = sinon.spy()

  const props = {
    airportChangeSearch,
    highlightNext,
    highlightPrevious,
    blurSearch,
    focusSearch,

    suggestions: [],
    highlightedSuggestion: 0,
    isShowingSuggestions: false
  }

  beforeEach(function () {
    airportChangeSearch.reset()
    highlightNext.reset()
    highlightPrevious.reset()
  })

  const component = <SearchBar {...props} />

  describe('handleChange', function () {
    it('calls airportSearchChange with the new search value', function () {
      const tree = sd.shallowRender(component)
      tree.subTree('SearchBarComponent').props.handleChange()

      assert(airportChangeSearch.calledOnce, 'The action was not dispatch exactly once but: ' + airportChangeSearch.callCount)
    })
  })

  describe('highlightNext', function () {
    it('calls highlightNext', function () {
      const tree = sd.shallowRender(component)
      tree.subTree('SearchBarComponent').props.goDownSuggestions()

      assert(highlightNext.calledOnce, 'The action was not dispatch exactly once but: ' + highlightNext.callCount)
    })
  })

  describe('mapStateToProps', function () {
    it('maps', function () {
      const state = getInitialStoreState()

      const props = mapStateToProps(state)

      const subState = state.airportSearch
      assert.deepStrictEqual(props.suggestions, subState.suggestions)
      assert.strictEqual(props.suggestions, subState.suggestions)
      assert.strictEqual(props.highlightedSuggestion, subState.highlightedSuggestion)
    })

    it('sets isShowingSuggestions to true when currentSearch is not empty and suggestions is not empty', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = 'notEmpty'
      subState.suggestions = [ 'notEmpty' ]
      subState.hasFocus = true

      const props = mapStateToProps(state)

      assert(props.isShowingSuggestions, 'isShowingSuggestions should be true')
    })

    it('sets isShowingSuggestions to true when currentSearch is not empty, suggestions is empty and isWaitingForFirstSuggestions is false', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = 'a'
      subState.suggestions = []
      subState.isWaitingForFirstSuggestions = false
      subState.hasFocus = true

      const props = mapStateToProps(state)

      assert(props.isShowingSuggestions, 'isShowingSuggestions should be true')
    })

    it('sets isShowingSuggestions to false when currentSearch is not empty, suggestions is empty and isWaitingForFirstSuggestions is true', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = 'a'
      subState.suggestions = []
      subState.isWaitingForFirstSuggestions = true
      subState.hasFocus = true

      const props = mapStateToProps(state)

      assert.strictEqual(props.isShowingSuggestions, false, 'isShowingSuggestions should be false')
    })

    it('sets isShowingSuggestions to true when currentSearch is not empty, suggestions is not empty and isWaitingForFirstSuggestions is true', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = 'a'
      subState.suggestions = [ 'a', 'b' ]
      subState.isWaitingForFirstSuggestions = true
      subState.hasFocus = true

      const props = mapStateToProps(state)

      assert.strictEqual(props.isShowingSuggestions, true, 'isShowingSuggestions should be false')
    })

    it('sets isShowingSuggestions to false when currentSearch is empty', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = ''
      subState.suggestions = [ 'a', 'b' ]
      subState.hasFocus = true

      const props = mapStateToProps(state)

      assert.strictEqual(props.isShowingSuggestions, false, 'isShowingSuggestions should be false')
    })

    it('sets isShowingSuggestions to true only if has focus', function () {
      const state = getInitialStoreState()
      const subState = state.airportSearch

      subState.currentSearch = 'notEmpty'
      subState.suggestions = [ 'a', 'b' ]
      subState.hasFocus = false

      const props = mapStateToProps(state)

      assert.strictEqual(props.isShowingSuggestions, false)
    })
  })
})
