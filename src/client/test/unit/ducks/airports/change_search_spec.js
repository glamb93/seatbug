'use strict'

/* global describe, beforeEach, it */

import assert from 'assert'
import deepFreeze from 'deep-freeze'
import sinon from 'sinon'

import { setApi } from '~/client/api'
import reducer, {
  airportChangeSearch,
  blurSearch,
  changeCurrentSearch,
  focusSearch,
  highlightNext,
  highlightPrevious,
  matchSuggestions,
  highlightSuggestion
} from '~/client/ducks/airports/change_search'

describe('Airport Change Search duck', function () {
  const someSearchText = 'blahblabla!'
  const matchingSuggestions = [ 'a', 'b' ]

  describe('airportChangeSearch thunk', function () {
    const dispatch = sinon.spy()
    let apiMock

    beforeEach(function () {
      dispatch.reset()
      apiMock = {}
      setApi(apiMock)
    })

    const thunk = airportChangeSearch(someSearchText)

    it('dispatches a change in search text', async function () {
      apiMock.post = sinon.stub().returns(Promise.resolve({ airports: matchingSuggestions }))
      await thunk(dispatch)
      assert(dispatch.calledWith(changeCurrentSearch(someSearchText)), 'The thunk should have dispatched a changeCurrentSearch action with the new search value')
    })

    it('dispatches a MATCH_SUGGESTIONS action when it receives the server response', async function () {
      apiMock.post = sinon.stub().returns(Promise.resolve({ airports: matchingSuggestions }))
      await thunk(dispatch)
      assert(dispatch.calledWith(matchSuggestions(someSearchText, matchingSuggestions)), 'Did not dispatched with the right action')
    })
  })

  describe('Reducer', function () {
    describe('CHANGE_SEARCH', function () {
      const action = changeCurrentSearch(someSearchText)

      it('handles undefined state', function () {
        assert.ok(reducer(undefined, action), 'The reducer does not return a valid state')
      })

      it('changes the currentSearch', function () {
        const initialState = {}
        deepFreeze(initialState)
        const nextState = reducer(initialState, action)
        assert.deepStrictEqual(nextState.currentSearch, someSearchText)
      })

      it('does not change areFirstSuggestionsReceived searchText is not empty ', function () {
        const nextState = reducer(undefined, action)
        assert.strictEqual(nextState.areFirstSuggestionsReceived, undefined)
      })

      it('sets isWaitingForFirstSuggestions to true if searchText is empty ', function () {
        const action = changeCurrentSearch('')
        const nextState = reducer(undefined, action)
        assert.strictEqual(nextState.isWaitingForFirstSuggestions, true)
      })
    })

    describe('MATCH_SUGGESTIONS', function () {
      const action = matchSuggestions(someSearchText, matchingSuggestions)

      it('handles undefined state', function () {
        assert.ok(reducer(undefined, action), 'The reducer does not return a valid state')
      })

      it('set isWaitingForFirstSuggestions to false if searchText correspond', function () {
        const initialState = { currentSearch: someSearchText }
        deepFreeze(initialState)

        const nextState = reducer(initialState, action)

        assert.strictEqual(nextState.isWaitingForFirstSuggestions, false, 'The reducer does not set the areFirstSuggestionsReceived')
      })

      it('changes the suggestions if the searchText correspond with the currentSearch', function () {
        const initialState = { currentSearch: someSearchText }
        deepFreeze(initialState)

        const nextState = reducer(initialState, action)

        const expected = matchingSuggestions
        assert.deepStrictEqual(nextState.suggestions, expected)
      })

      it('sets the highlightedSuggestion to 0', function () {
        const initialState = {
          currentSearch: someSearchText,
          highlightedSuggestion: 2
        }
        deepFreeze(initialState)
        const nextState = reducer(initialState, action)
        assert.strictEqual(nextState.highlightedSuggestion, 0)
      })

      it('keeps the state intact if the searchText is different to the currentSearch', function () {
        const initialState = { currentSearch: 'someOtherSearchText' }
        deepFreeze(initialState)
        const nextState = reducer(initialState, action)
        assert.deepStrictEqual(nextState, initialState)
      })
    })

    describe('HIGHLIGHT_NEXT', function () {
      const action = highlightNext()

      it('handles empty state', function () {
        assert.ok(reducer(undefined, action))
      })

      it('handles empty suggestions', function () {
        const initialState = {
          suggestions: [],
          highlightedSuggestion: 2
        }
        assert.ok(reducer(initialState, action))
      })

      it('increments the highlightedSuggestions', function () {
        const initialState = {
          suggestions: [ 'a', 'b', 'c', 'd' ],
          highlightedSuggestion: 2
        }
        const nextState = reducer(initialState, action)

        assert.strictEqual(nextState.highlightedSuggestion, initialState.highlightedSuggestion + 1)
      })

      it('wrap the suggestions when highlightedSuggestion is bigger than the number of suggestions - 1', function () {
        const initialState = {
          suggestions: [ 'a', 'b' ],
          highlightedSuggestion: 1
        }
        const nextState = reducer(initialState, action)

        assert.strictEqual(nextState.highlightedSuggestion, 0)
      })
    })

    describe('HIGHLIGHT_PREVIOUS', function () {
      const action = highlightPrevious()

      it('handles empty state', function () {
        assert.ok(reducer(undefined, action))
      })

      it('handles empty suggestions', function () {
        const initialState = {
          suggestions: [],
          highlightedSuggestion: 2
        }
        assert.ok(reducer(initialState, action))
      })

      it('decrements the highlightedSuggestions', function () {
        const initialState = {
          suggestions: [ 'a', 'b', 'c', 'd' ],
          highlightedSuggestion: 2
        }
        const nextState = reducer(initialState, action)

        assert.strictEqual(nextState.highlightedSuggestion, initialState.highlightedSuggestion - 1)
      })

      it('wraps the suggestions when highlightedSuggestion is lower than the number of suggestions - 1', function () {
        const initialState = {
          suggestions: [ 'a', 'b', 'c' ],
          highlightedSuggestion: 0
        }
        const nextState = reducer(initialState, action)

        assert.strictEqual(nextState.highlightedSuggestion, initialState.suggestions.length - 1)
      })
    })

    describe('BLUR_SEARCH', function () {
      const action = blurSearch()

      it('handles empty state', function () {
        assert.ok(reducer(undefined, action))
      })

      it('sets hasFocus to false', function () {
        const initialState = {
          hasFocus: true
        }
        const nextState = reducer(initialState, action)
        assert.strictEqual(nextState.hasFocus, false)
      })
    })

    describe('FOCUS_SEARCH', function () {
      const action = focusSearch()

      it('handles undefined state', function () {
        assert.ok(reducer(undefined, action))
      })

      it('sets hasFocus to true', function () {
        const initialState = {
          hasFocus: false
        }
        const nextState = reducer(initialState, action)
        assert.strictEqual(nextState.hasFocus, true)
      })
    })

    describe('HIGHLIGHT_SUGGESTION', function () {
      const anAirportCode = 'BBB'
      const action = highlightSuggestion(anAirportCode)

      it('handles undefined state', function () {
        assert.ok(reducer(undefined, action))
      })

      it('change the highlightedSuggestion for the new one', function () {
        const initialState = {
          suggestions: [ { code: 'AAA' }, { code: 'BBB' } ],
          highlightedSuggestion: '0'
        }
        const nextState = reducer(initialState, action)
        assert.strictEqual(nextState.highlightedSuggestion, 1)
      })
    })
  })
})
