'use strict'

/* global describe, beforeEach, it */

import assert from 'assert'
import sinon from 'sinon'

import React from 'react'

import sd from '~/client/test/lib/skin_deep_wrapper'
import { should } from '~/client/test/unit/test_helper'

import SearchBarComponent from '~/client/components/high_orders/search_bar_component'

const UP_ARROW_CODE = 38
const DOWN_ARROW_CODE = 40

describe('Search Bar Component', function () {
  const handleChange = sinon.spy()
  const goDownSuggestions = sinon.spy()
  const goUpSuggestions = sinon.spy()
  const handleBlurSearch = sinon.spy()
  const handleFocusSearch = sinon.spy()

  const INITIAL_PROPS = {
    handleChange,
    handleBlurSearch,
    handleFocusSearch,
    goDownSuggestions,
    goUpSuggestions,

    isShowingSuggestions: false,
    suggestions: [],
    highlightedSuggestion: 0
  }

  beforeEach(function () {
    handleChange.reset()
    goDownSuggestions.reset()
    goUpSuggestions.reset()
    handleBlurSearch.reset()
    handleFocusSearch.reset()
  })

  it('display a input box', function () {
    const component = <SearchBarComponent { ...INITIAL_PROPS }/>
    should.renderRef(component, 'input')
  })

  it('calls onChangeHandler when the input change', function () {
    const component = <SearchBarComponent { ...INITIAL_PROPS }/>
    const tree = sd.shallowRender(component)
    const node = tree.ref('input')

    const newValue = 'blabla'
    const event = { target: { value: newValue } }
    node.props.onChange(event)

    assert(handleChange.calledOnce, 'The handler was not called once but: ' + handleChange.callCount)
    assert(handleChange.calledWith(newValue), 'The handler was not called with the new value of the input')
  })

  it('displays suggestions when isShowingSuggestions', function () {
    const props = {
      ...INITIAL_PROPS,
      isShowingSuggestions: true
    }
    const component = <SearchBarComponent { ...props }/>
    should.renderComponent(component, 'Suggestions')
  })

  it('does not display suggestions when not isShowingSuggestions', function () {
    const props = {
      ...INITIAL_PROPS,
      isShowingSuggestions: false
    }
    const component = <SearchBarComponent { ...props }/>
    should.notRenderComponent(component, 'Suggestions')
  })

  it('calls the appropriate callback when DOWN_ARROW is pressed', function () {
    const props = {
      ...INITIAL_PROPS,
      goDownSuggestions
    }
    const component = <SearchBarComponent {...props} />
    const searchInput = should.getRef(component, 'input')

    const event = { keyCode: DOWN_ARROW_CODE }
    searchInput.props.onKeyDown(event)

    assert(goDownSuggestions.calledOnce, 'The callback was not called only once')
  })

  it('calls the appropriate callback when UP_ARROW is pressed', function () {
    const props = {
      ...INITIAL_PROPS,
      goUpSuggestions
    }
    const component = <SearchBarComponent {...props} />
    const searchInput = should.getRef(component, 'input')

    const event = { keyCode: UP_ARROW_CODE }
    searchInput.props.onKeyDown(event)

    assert(goUpSuggestions.calledOnce, 'The callback was not called only once but: ' + goUpSuggestions.callCount)
  })

  it('calls the appropriate callback when mouse enter a suggestion', function () {
    const props = {
      ...INITIAL_PROPS,
      goUpSuggestions
    }
    const component = <SearchBarComponent {...props} />
    const searchInput = should.getRef(component, 'input')

    const event = { keyCode: UP_ARROW_CODE }
    searchInput.props.onKeyDown(event)

    assert(goUpSuggestions.calledOnce, 'The callback was not called only once but: ' + goUpSuggestions.callCount)
  })

  it('calls the appropriate callback when input loses the focus', function () {
    const component = <SearchBarComponent {...INITIAL_PROPS} />
    const searchInput = should.getRef(component, 'input')

    searchInput.props.onBlur()

    assert(handleBlurSearch.calledOnce, 'The callback was not called only once but: ' + handleBlurSearch.callCount)
  })
})
