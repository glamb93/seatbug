'use strict'

/* global describe, it */

import assert from 'assert'

import React from 'react'

import { should } from '~/client/test/unit/test_helper'
import Suggestions from '~/client/components/suggestions'

describe('Suggestions Component', function () {
  const INITIAL_PROPS = {
    isWaitingForFirstSuggestions: true,
    suggestions: [],
    highlightedSuggestion: 0
  }

  it('displays a "No suggestions" element when searchingFor something and there is no suggestions', function () {
    const props = {
      ...INITIAL_PROPS,
      searchingFor: 'IMPOSIBLETextInHere'
    }
    const component = <Suggestions {...props} />
    should.renderRef(component, 'noSuggestionsElement')
  })

  it('displays the correct highlighted suggestion', function () {
    const props = {
      ...INITIAL_PROPS,
      searchingFor: 'a',
      highlightedSuggestion: 2,
      suggestions: [
        { code: 'a', name: 'a' },
        { code: 'a', name: 'b' },
        { code: 'a', name: 'c' },
        { code: 'a', name: 'd' }
      ]
    }
    const component = <Suggestions {...props} />
    const highlightedSuggestion = should.getRef(component, 'highlightedSuggestion')
    assert(highlightedSuggestion.props.isHighlighted, 'Highlighting situation knowledge is not transfered to the child')
  })
})
