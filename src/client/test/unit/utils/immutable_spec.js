'use strict'

/* global describe, it */

import assert from 'assert'
import deepFreeze from 'deep-freeze'

import { remove } from '~/client/utils/immutable'

describe('Immutable helpers', function () {
  describe('removeOne()', function () {
    const anArray = [1, 2, 3, 4]

    const aSelector = (e) => e % 2 === 0

    it('keep the input array unchanged', function () {
      deepFreeze(anArray)
      remove(anArray, aSelector)
    })

    it('remove all the array elements that make the selector function return true', function () {
      const result = remove(anArray, aSelector)
      assert.deepStrictEqual(result, [1, 3, 4])
    })
  })
})

