'use strict'

import assert from 'assert'

import '~/client/test/setup_fake_dom'
import sd from '~/client/test/lib/skin_deep_wrapper'

export { getInitialStoreState } from '../../../client/store'

export const should = {
  callHandlerOnceOnClick,
  getRef,
  renderComponent,
  renderRef,
  notRenderComponent,
  notRenderRef
}

function getRef (component, ref) {
  return renderRef(component, ref)
}

function renderComponent (componentToRender, componentNameToLookUp) {
  const tree = sd.shallowRender(componentToRender)
  const subTree = tree.subTree(componentNameToLookUp)
  assert.ok(subTree, 'The component "' + componentNameToLookUp + '" was not found')
}

function renderRef (component, ref) {
  const tree = sd.shallowRender(component)
  const node = tree.ref(ref)
  assert.ok(node, 'The component did not render a "' + ref + '" ref')
  return node
}

function notRenderComponent (componentToRender, componentNameToLookUp) {
  const tree = sd.shallowRender(componentToRender)
  const subTree = tree.subTree(componentNameToLookUp)
  assert.ok(!subTree, 'The component "' + componentNameToLookUp + '" should not be found')
}

function notRenderRef (component, ref) {
  const tree = sd.shallowRender(component)
  const node = tree.ref(ref)
  assert.ok(!node, 'The ref "' + ref + '" should not be found')
}

function callHandlerOnceOnClick (component, ref, handlerSpy) {
  const tree = sd.shallowRender(component)
  const node = tree.ref(ref)

  node.props.onClick()

  assert(handlerSpy.calledOnce, 'The handler of "' + ref + '" click was not called exactly once but: ' + handlerSpy.callCount)
}
