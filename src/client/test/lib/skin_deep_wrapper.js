'use strict'

import React from 'react'
import sd from 'skin-deep'

export default {
  shallowRender: function (component, context) {
    const tree = sd.shallowRender(component, context)
    return wrapTree(tree)
  }
}

function wrapTree (tree) {
  return {
    props: tree.props,
    subTree: wrapFunction(tree.subTree),
    everySubTree: wrapFunction(sd.everySubTree),
    ref: (ref) => getRefInTree(tree, ref)
  }
}

function wrapFunction (fn) {
  return function (...args) {
    const tree = fn(...args)

    if (tree) {
      return wrapTree(tree)
    } else {
      return false
    }
  }
}

function getRefInTree (node, ref) {
  // Falsy stuff can't match or have children
  if (!node) {
    return false
  }

  if (Array.isArray(node)) {
    var result = findRefAmongArrayEntries(node, ref)
    if (result) return result
  } else {
    if (node.ref === ref) {
      return node
    }

    if (!isAReactComponent(node)) {
      return findRefAmongChildren(node, ref)
    }
  }

  return false
}

function findRefAmongArrayEntries (array, ref) {
  var found = false
  for (var i = 0; i < array.length; i++) {
    var node = array[i]
    found = getRefInTree(node, ref)
    if (found) {
      return found
    }
  }
  return found
}

function findRefAmongChildren (node, ref) {
  if (hasChildren(node)) {
    var children = childrenArray(node.props.children)
    return getRefInTree(children, ref)
  }
  return false
}

function hasChildren (node) {
  return node.props && node.props.children
}

function childrenArray (children) {
  var array = []
  React.Children.forEach(children, function (child) {
    array.push(child)
  })
  return array
}

function isAReactComponent (node) {
  return typeof node.type === 'function'
}
