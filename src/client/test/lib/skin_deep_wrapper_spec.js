'use strict'

/* global describe, it */

import assert from 'assert'

import React from 'react'

import sd from './skin_deep_wrapper'

describe('Skin Deep Wrapper', function () {
  describe('Refs', function () {
    const aReactComponentChildRef = 'childrenRef'
    const aDOMElementChildRef = 'childrenRef2'
    const aRootRef = 'rootRef'
    const anUnexistingRef = 'aSillyRef'
    const aDoubleRef = 'imUsedTwiceForTheSameComponent'
    const rootElementRef = 'rootElementRef'

    const ComponentWithChildrenRefs = React.createClass({
      render: function () {
        return <h1 ref={aReactComponentChildRef}>Allo</h1>
      }
    })

    const RootComponent = React.createClass({
      render: function () {
        return (
          <div ref={rootElementRef}>
            <ComponentWithChildrenRefs><h2 ref='test' /></ComponentWithChildrenRefs>

            <h1 ref={aDoubleRef} />
            <h2 ref={aDoubleRef} />

            <div ref={aRootRef}>
              <h3 ref={aDOMElementChildRef} />
            </div>

          </div>
        )
      }
    })

    const tree = sd.shallowRender(<RootComponent />)

    it.skip('throws an error if it find more than one ref', function () {
      assert.throws(() => tree.ref(aDoubleRef), Error, 'An error should have been thrown')
    })

    it('returns false if the root does not have any element with the given ref', function () {
      const result = tree.ref(anUnexistingRef)
      assert(!result, 'Should return false')
    })

    it('returns the subtree of the requested ref at immediate level', function () {
      const result = tree.ref(aRootRef)
      assert.strictEqual(result.ref, aRootRef)
    })

    it('returns the subtree of the requested ref even at DOM-nested-level', function () {
      const result = tree.ref(aDOMElementChildRef)
      assert.strictEqual(result.ref, aDOMElementChildRef)
    })

    it('does not return the ref of the root react component children even if they match the requested value', function () {
      const result = tree.ref(aReactComponentChildRef)
      assert(!result, 'Does not return false')
    })
  })

  describe('WrappedSubTree', function () {
    it('does not wrap subTree return value if  subTree returns false', function () {
      class SomeComponent extends React.Component {
        render () {
          return 'Nothing really important'
        }
      }

      const tree = sd.shallowRender(<SomeComponent />)
      const subTree = tree.subTree('should not be found')

      assert(!subTree, 'SubTree should be false')
    })
  })
})
