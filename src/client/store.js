'use strict'

import {
  routerMiddleware,
  routerReducer
} from 'react-router-redux'
import { combineReducers } from 'redux'
import {
  applyMiddleware,
  compose,
  createStore
} from 'redux'
import { reducer as formReducer } from 'redux-form'
import thunkMiddleware from 'redux-thunk'

import reducers from '~/client/ducks/reducers'

let storeInstance
let history

export const getStore = function () {
  if (!storeInstance) {
    initializeStore()
  }
  return storeInstance
}

export const setHistory = (routerHistory) => { history = routerHistory }

export function initializeStore (initialState) {
  if (!history) {
    throw new Error('No history has been set yet')
  }

  const rootReducer = reducerEnhancer(reducers)
  storeInstance = createStore(rootReducer, initialState, getStoreEnhancer())

  if (module.hot) {
    enableWebpackReducerHotReload(storeInstance)
  }

  return storeInstance
}

export function getInitialStoreState () {
  const reducer = reducerEnhancer(reducers)
  const state = reducer(undefined, {})
  return state
}

function reducerEnhancer (reducers) {
  return combineReducers({
    ...reducers,
    form: formReducer,
    routing: routerReducer
  })
}

function getStoreEnhancer () {
  return compose(
    applyMiddleware(thunkMiddleware),
    applyMiddleware(routerMiddleware(history)),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f
  )
}

function enableWebpackReducerHotReload (store) {
  module.hot.accept('./ducks/reducers', () => {
    const nextReducers = require('./ducks/reducers').default
    const nextRootReducer = reducerEnhancer(nextReducers)

    store.replaceReducer(nextRootReducer)
  })
}
