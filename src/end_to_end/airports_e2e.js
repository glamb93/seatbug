'use strict'

/* global describe, before, after afterEach */

import assert from 'assert'
import {
  findRenderedComponentWithType,
  scryRenderedDOMComponentsWithTag,
  scryRenderedComponentsWithType,
  Simulate
} from 'react-addons-test-utils'

import React from 'react'

import {
  asyncIt,
  renderApp,
  Test,
  when
} from '~/end_to_end/helpers/test_utils'
import {
  startBackendApp,
  Store
} from '~/end_to_end/helpers/essentials'
import populateDatabase from '~/server/data/populate_database'

import SearchBarComponent from '~/client/components/high_orders/search_bar_component'
import Suggestion from '~/client/components/suggestion'
import Suggestions from '~/client/components/suggestions'

const DOWN_ARROW_KEY_EVENT = { keyCode: 40 }
const UP_ARROW_KEY_EVENT = { keyCode: 38 }

describe('Airport Resource', function () {
  const moreThanOneSuggestionSearch = 'Y'

  let app

  before(function () {
    app = startBackendApp()
    populateDatabase()
  })

  after(function () {
    app.stop()
  })

  afterEach(function () {
    Store.resetStore()
  })

  asyncIt('prevents the "No Suggestions" element to show up before any text is entered', async function (done) {
    const app = renderApp()

    new Test(done)
    .component(app)
    .expectNotToRenderComponentType(Suggestions)
  })

  asyncIt('allows to write in the searchbar and see the suggestions element', async function (done) {
    const app = renderApp()
    const searchBar = getSearchBar(app)

    new Test(done)
    .component(searchBar)
    .when(() => searchBar.props.isShowingSuggestions)
    .expectToRender(<ul ref='suggestions' />)
    .trigger(() => changeSearchBarText(app, 'blahblabla'))
  })

  asyncIt('allows to write in the searchbar and see the "No Suggestion" element after receiving the server response', async function (done) {
    const impossibleAirport = 'the intergalactic airport of the Central Universe East'
    const app = renderApp()
    const searchBar = getSearchBar(app)

    new Test(done)
    .component(searchBar)
    .when(() => searchBar.props.isShowingSuggestions)
    .expectToRender(
      <ul ref='suggestions'>
        <li ref='noSuggestionElement' />
      </ul>
    )
    .trigger(() => changeSearchBarText(app, impossibleAirport)
            )
  })

  asyncIt('allows to write in the searchbar and not see the "No Suggestion" element before receiving the server response', async function (done) {
    const someSearch = 'taipei'
    const app = renderApp()
    const searchBar = getSearchBar(app)

    new Test(done)
    .when(when.FIRST_RENDER)
    .component(searchBar)
    .expectNoRef('noSuggestionsElement')
    .trigger(() => changeSearchBarText(app, someSearch))
  })

  asyncIt('allows to write and see the first suggestion highlighted', async function (done) {
    const searchTextWithSuggestions = 'taiwan'
    const app = renderApp()
    const searchBar = getSearchBar(app)

    new Test(done)
    .component(() => findRenderedComponentWithType(app, Suggestions))
    .when(() => searchBar.props.suggestions.length > 0)
    .hookedOn(searchBar)
    .expectRenderedRef('highlightedSuggestion')
    .trigger(() => changeSearchBarText(app, searchTextWithSuggestions))
  })

  asyncIt('can highlight the second suggestion', async function (done) {
    const app = renderApp()

    await changeSearchBarTextAndWaitForSuggestions(app, moreThanOneSuggestionSearch)
    await pressKeyAndWaitSuggestionHighlightChange(app, DOWN_ARROW_KEY_EVENT)

    const suggestionsComponent = getSuggestionsElement(app)
    const suggestionsProp = suggestionsComponent.props.suggestions
    const expected = suggestionsProp[1]
    const actual = suggestionsComponent.refs.highlightedSuggestion

    assert.deepStrictEqual(actual.props.airport, expected)
    done()
  })

  asyncIt('can highlight the last suggestion', async function (done) {
    const app = renderApp()

    await changeSearchBarTextAndWaitForSuggestions(app, moreThanOneSuggestionSearch)
    await pressKeyAndWaitSuggestionHighlightChange(app, UP_ARROW_KEY_EVENT)

    const suggestionsElement = getSuggestionsElement(app)
    const suggestionsProp = suggestionsElement.props.suggestions
    const expected = suggestionsProp[suggestionsProp.length - 1]
    const actual = suggestionsElement.refs.highlightedSuggestion
    assert.deepStrictEqual(actual.props.airport, expected)
    done()
  })

  asyncIt('hides the suggestions when the searchInput looses focus', async function (done) {
    const app = renderApp()
    const searchBar = getSearchBar(app)

    await changeSearchBarTextAndWaitForSuggestions(app, moreThanOneSuggestionSearch)

    new Test(done)
    .component(searchBar)
    .when(() => !searchBar.props.isShowingSuggestions)
    .expectNoRef('suggestions')
    .trigger(() => Simulate.blur(searchBar.refs.input))
  })

  asyncIt('retrieves the suggestions of the current text when the focus is set back to the input', async function (done) {
    const app = renderApp()
    const searchBar = getSearchBar(app)

    await changeSearchBarTextAndWaitForSuggestions(app, moreThanOneSuggestionSearch)
    const suggestionsSnapShot = [ ...searchBar.props.suggestions ]
    await blurSearchInputAndWaitThatSuggestionsDisappear(app)

    new Test(done)
    .component(searchBar)
    .when(() => searchBar.props.isShowingSuggestions)
    .customTest(() => assert.deepStrictEqual(searchBar.props.suggestions, suggestionsSnapShot))
    .trigger(() => Simulate.focus(searchBar.refs.input))
  })

  asyncIt('highlights the hovered suggestion', async function (done) {
    const app = renderApp()

    await changeSearchBarTextAndWaitForSuggestions(app, moreThanOneSuggestionSearch)
    const suggestionsElement = findRenderedComponentWithType(app, Suggestions)
    const suggestionsDOM = scryRenderedDOMComponentsWithTag(suggestionsElement, 'li')
    const secondSuggestionDOM = suggestionsDOM[1]

    const suggestionComponents = scryRenderedComponentsWithType(suggestionsElement, Suggestion)
    const secondSuggestionComponent = suggestionComponents[1]

    new Test(done)
    .component(suggestionsElement)
    .when(when.FIRST_RENDER)
    .customTest(function () {
      assert.deepStrictEqual(
        secondSuggestionComponent.props,
        suggestionsElement.refs.highlightedSuggestion.props
      )
    })
    .trigger(() => Simulate.mouseEnter(secondSuggestionDOM))
  })
})

function getSearchBar (app) {
  return findRenderedComponentWithType(app, SearchBarComponent)
}

function getSuggestionsElement (app) {
  return findRenderedComponentWithType(app, Suggestions)
}

function changeSearchBarTextAndWaitForSuggestions (app, newText) {
  return new Promise(function (resolve, reject) {
    const searchBar = getSearchBar(app)

    searchBar.componentDidUpdate = function () {
      if (this.props.suggestions.length > 0) {
        resolve()
      }
    }

    changeSearchBarText(app, newText)
  })
}

function changeSearchBarText (app, newText) {
  const searchBar = getSearchBar(app)
  Simulate.focus(searchBar.refs.input)
  Simulate.change(searchBar.refs.input, { target: { value: newText } })
}

function pressKeyAndWaitSuggestionHighlightChange (app, keyEvent) {
  return new Promise(function (resolve, reject) {
    const searchBar = getSearchBar(app)
    const previousHighlight = searchBar.props.highlightedSuggestion

    searchBar.componentDidUpdate = function () {
      if (this.props.highlightedSuggestion !== previousHighlight) {
        resolve()
      }
    }

    Simulate.keyDown(searchBar.refs.input, keyEvent)
  })
}

function blurSearchInputAndWaitThatSuggestionsDisappear (app) {
  return new Promise(function (resolve, reject) {
    const searchBar = getSearchBar(app)

    searchBar.componentDidUpdate = function () {
      if (!searchBar.props.isShowingSuggestions) {
        resolve()
      }
    }

    Simulate.blur(searchBar.refs.input)
  })
}
