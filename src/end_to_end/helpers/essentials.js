'use strict'

import BackendApp from '~/server/app'
import Mongodb, { generateId } from '~/server/db/mongodb'

import { initializeApi } from '~/client/api'
import * as _Store from '~/client/store'

const API_END_TO_END_URL = 'http://localhost:3032'
const DB_CONNECTION_URL = 'mongodb://127.0.0.1:27017/cookies-end-to-end'

export const startBackendApp = BackendApp

export const db = {
  ...Mongodb(DB_CONNECTION_URL),
  generateId
}

export const getStore = _Store.getStore
export const resetStore = () => _Store.initializeStore()
export const initializeStore = _Store.initializeStore
export const getRoute = () => _Store.getStore().getState().routing.locationBeforeTransitions.pathname

export const Store = {
  getStore,
  resetStore,
  initializeStore,
  getRoute
}

initializeApi(API_END_TO_END_URL)
