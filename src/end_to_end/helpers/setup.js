'use strict'

import 'babel-polyfill'

import '~/end_to_end/helpers/setup_fake_dom'
import 'unexpected'
import 'unexpected-react'
