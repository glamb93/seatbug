'use strict'

import {MongoClient, ObjectID} from 'mongodb'

export default function Database (dbConnectionUrl) {
  return {
    collection: (collectionName) => Collection(collectionName, dbConnectionUrl)
  }
}

export function generateId () {
  return new ObjectID().toString()
}

function Collection (collectionName, dbConnectionUrl) {
  const COLLECTION_NAME = collectionName
  const DB_CONNECTION_URL = dbConnectionUrl

  return {
    get,
    insert,
    insertMany,
    remove,
    findAll,
    count,
    clear
  }

  async function get (id) {
    let filter = { _id: new ObjectID(id) }
    let result = await execute(async function(db) {
      return await db.collection(COLLECTION_NAME).findOne(filter)
    })

    return result ? fromDBO(result) : null
  }

  async function insert (doc) {
    let docClone = { ...doc }

    let result = await execute(async function (db) {
      return await db.collection(COLLECTION_NAME).insert(docClone)
    })

    let insertedDoc = result.ops[0]

    return fromDBO(insertedDoc)
  }

  async function insertMany (docs) {
    let docsClone = []
    docs.map((d) => docsClone.push({ ...d }))

    let result = await execute(async function (db) {
      return await db.collection(COLLECTION_NAME).insertMany(docsClone)
    })

    const insertedDocs = result.ops

    return manyFromDBO(insertedDocs)
  }

  async function remove (id) {
    await execute(async function (db) {
      await db.collection(COLLECTION_NAME).remove({ _id: new ObjectID(id) })
    })
  }

  async function findAll (filter) {
    let documents = await execute(async function (db) {
      return await db.collection(COLLECTION_NAME).find(filter).toArray()
    })
    return manyFromDBO(documents)
  }

  async function count () {
    let count = await execute(async function (db) {
      return await db.collection(COLLECTION_NAME).count()
    })
    return count
  }

  async function clear () {
    await execute(async function (db) {
      db.collection(COLLECTION_NAME).remove()
    })
  }

  async function execute (ops) {
    try {
      let db = await MongoClient.connect(DB_CONNECTION_URL)
      let result = await ops(db)
      db.close()
      return result
    } catch (err) {
    }
  }

  function manyFromDBO (dbos) {
    return dbos.map((d) => fromDBO(d))
  }

  function fromDBO (dbo) {
    let element = { ...dbo }

    if (element) {
      element.id = element._id.toString()
      delete element._id
    }

    return element
  }
}
