'use strict'

import mongodb2_4 from './mongodb2.4'
import mongodb3_2 from './mongodb3.2'

export default process.env.OPENSHIFT_MONGODB_DB_USERNAME ? mongodb2_4 : mongodb3_2
