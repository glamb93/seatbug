'use strict'

import assert from 'assert'
import {MongoClient, ObjectID} from 'mongodb'

export default function Database (dbConnectionUrl) {
  return {
    collection: (collectionName) => Collection(collectionName, dbConnectionUrl)
  }
}

export function generateId () {
  return new ObjectID().toString()
}

function Collection (collectionName, dbConnectionUrl) {
  const COLLECTION_NAME = collectionName
  const DB_CONNECTION_URL = dbConnectionUrl

  return {
    get,
    insert,
    insertMany,
    remove,
    findAll,
    count,
    clear
  }

  function get (id) {
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).findOne({ _id: new ObjectID(id) }, function (err, result) {
        assert.equal(err, null)
        db.close()

        if (result) {
          resolve(fromDBO(result))
        } else {
          resolve(null)
        }
      })
    })
  }

  function insert (ingredient) {
    let doc = { ...ingredient }
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).insert(doc, function (err, result) {
        assert.equal(err, null)
        db.close()

        let insertedIngredient = fromDBO(doc)
        resolve(insertedIngredient)
      })
    })
  }

  async function insertMany (docs) {
    let docClones = []
    docs.map((d) => docClones.push({ ...d }))

    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).insert(docClones, {}, function (err, result) {
        assert.equal(err, null)
        db.close()

        let readyToReturnDocs = manyFromDBO(docClones)
        resolve(readyToReturnDocs)
      })
    })
  }

  function remove (id) {
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).remove({ _id: new ObjectID(id) }, function (err) {
        assert.equal(err, null)

        db.close()
        resolve()
      })
    })
  }

  function findAll (filterObject) {
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      let cursor = db.collection(COLLECTION_NAME).find(filterObject)
      cursor.toArray(function (err, result) {
        assert.equal(err, null)

        db.close()
        result = manyFromDBO(result)
        resolve(result)
      })
    })
  }

  function count () {
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).count(function (err, count) {
        assert.equal(err, null)

        db.close()
        resolve(count)
      })
    })
  }

  async function clear () {
    return new Promise(async function (resolve, reject) {
      let db = await connect()

      db.collection(COLLECTION_NAME).remove({}, function (err) {
        assert.equal(err, null)

        db.close()
        resolve()
      })
    })
  }

  function connect () {
    return new Promise(function (resolve, reject) {
      MongoClient.connect(DB_CONNECTION_URL, function (err, db) {
        assert.equal(err, null)
        resolve(db)
      })
    })
  }

  function manyFromDBO (dbos) {
    return dbos.map((d) => fromDBO(d))
  }

  function fromDBO (dbo) {
    let element = { ...dbo }

    if (element) {
      element.id = element._id.toString()
      delete element._id
    }

    return element
  }
}
