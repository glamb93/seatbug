'use strict'

const MAX_N_OF_SUGGESTIONS_TO_RETURN = 6

export default function addAirportRessource (server, db) {
  server.post('/airports', async function (req, res) {
    const searchText = req.body.search

    const matchingAirports = await matchAirportsWithRegex(db, searchText)
    const limitedResult = limitResults(matchingAirports, MAX_N_OF_SUGGESTIONS_TO_RETURN)

    res.json({ airports: limitedResult })
  })

  return server
}

function limitResults (suggestions, maximum) {
  return suggestions.slice(0, MAX_N_OF_SUGGESTIONS_TO_RETURN)
}

async function matchAirportsWithRegex (db, searchText) {
  const collection = db.collection('airports')
  const regex = new RegExp('(^|.*?\\s)' + searchText + '.*', 'i')

  return await collection.findAll({
    $or: [
      { code: regex },
      { name: regex },
      { city: regex },
      { country: regex }
    ]
  })
}
