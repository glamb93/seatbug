'use strict'

import express from 'express'
import bodyParser from 'body-parser'

import addAirportResourceTo from '~/server/resources/airports'
import serveStatic from 'serve-static'

export default function Server (appIpAdress, appPort, db, publicPath, withCrossOrigin = false) {
  let connection

  function start () {
    let server = express()

    server.use(serveStatic(publicPath, {'index': ['index.html']}))
    server.use(bodyParser.json())

    if (withCrossOrigin) {
      allowCrossOrigin(server)
    }

    addAirportResourceTo(server, db)

    connection = server.listen(appPort, appIpAdress, function () {
    })
  }

  function stop () {
    connection.close()
  }

  function allowCrossOrigin (server) {
    server.use(function (req, res, next) {
      res.header('Access-Control-Allow-Origin', '*')
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
      next()
    })
  }

  return {
    allowCrossOrigin,
    start: start,
    stop: stop
  }
}
