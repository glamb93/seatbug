'use strict'

import fs from 'fs'

import getAirportsOpenFlights from './prepare_openflights_airports'
import getAirportsTdReyno from './prepare_tdreyno_airports'

const PREPARED_FILE_PATH = './src/server/data/prepared_data/airport_data.json'
const PREPARED_E2E_FILE_PATH = './src/server/data/prepared_data/airport_e2e_data.json'

const E2E_REQUIRED_AIRPORT_CODES = [
  'YUL',
  'YQB',
  'JFK',
  'ARN',
  'TPE',
  'YYZ'
]

prepareData()

function prepareData () {
  const prodData = extractDataForProd()
  const simplifiedData = extractDataForE2E()

  fs.writeFileSync(PREPARED_FILE_PATH, JSON.stringify(prodData))
  fs.writeFileSync(PREPARED_E2E_FILE_PATH, JSON.stringify(simplifiedData))
}

function extractDataForProd () {
  const airports = getAirportsTdReyno()
  const otherVersionWithBetterCityName = getAirportsOpenFlights()

  otherVersionWithBetterCityName.map(function (a) {
    const homonym = airports.find((a1) => a1.code === a.code)
    if (homonym) {
      homonym.city = a.city
    } else {
      
  })

  return airports
}

function extractDataForE2E () {
  const e2eOutput = []

  const airportData = getAirportsTdReyno()
  airportData.map(function (airport) {
    if (isRelevantForE2E(airport)) {
      e2eOutput.push(airport)
    }
  })

  return e2eOutput
}

function isRelevantForE2E (extractedAirportData) {
  const code = extractedAirportData.code
  return !!E2E_REQUIRED_AIRPORT_CODES.find((c) => c === code)
}
