'use strict'

import rawAirportData from './raw_data/tdreyno_airports.json'

export default function extractReleventInfo (rawAirportInput) {
  const preparedAirport = []

  rawAirportData.map(function (a) {
    if (a.type === 'Airports') {
      preparedAirport.push({
        code: a.code,
        name: a.name,
        city: a.city,
        state: a.state,
        country: a.country,
        nDirectFlights: a.direct_flights
      })
    }
  })

  return preparedAirport
}
