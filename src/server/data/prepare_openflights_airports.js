'use strict'

import openFlightsData from './raw_data/openflights_airports.json'

export default function getAirport () {
  const output = []

  openFlightsData.map(function (a) {
    if (a.code) {
      output.push({
        code: a.code,
        name: a.name,
        city: a.city,
        country: a.country
      })
    }
  })

  return output
}

