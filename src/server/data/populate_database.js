'use strict'

export default async function populateDatabase (db, useE2EDataVersion) {
  const dataFileName = useE2EDataVersion ? 'airport_e2e_data.json' : 'airport_data.json'
  const data = require(`./prepared_data/${dataFileName}`)
  const collection = db.collection('airports')

  await collection.clear()
  await collection.insertMany(data)
}
