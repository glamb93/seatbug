'use strict'

/* global describe, before, after, it */

import assert from 'assert'

import fetch from '~/server/test/integration/fetch_helper'

import App from '~/server/app'
import Mongodb from '~/server/db/mongodb'

describe('Airport Resource', function () {
  let app

  const airportData = [
    { code: 'AAA', name: 'AAA ville Airport', city: 'AAA', state: 'AA', country: 'A' },
    { code: 'AAB', name: 'AAA ville Airport', city: 'AAA', state: 'AA', country: 'A' },
    { code: 'ACA', name: 'ACA ville Airport', city: 'ACA', state: 'AC', country: 'A' },
    { code: 'ZZZ', name: 'ZZZ ville Airport', city: 'ZZZ', state: 'AC', country: 'A' },
    { code: 'YYY', name: 'YYY ville Airport', city: 'YYY', state: 'AC', country: 'A' },
    { code: 'BBB', name: 'BBB ville Airport', city: 'BBB', state: 'BB', country: 'B' },
    { code: 'BZB', name: 'BZB ville Airport', city: 'BBB', state: 'BB', country: 'B' }
  ]

  before(async function () {
    const db = Mongodb('mongodb://127.0.0.1/seatbug-test')
    const collection = db.collection('airports')

    await collection.clear()
    await collection.insertMany(airportData)

    app = App(db)
  })

  after(function () {
    app.stop()
  })

  it('filters', async function () {
    const matchingAirports = await requestSuggestions('AAA')

    const expected = [
      airportData[0],
      airportData[1]
    ]

    assert.ok(matchingAirports.find((a) => a.name === expected[0].name))
    assert.ok(matchingAirports.find((a) => a.name === expected[1].name))
    assert.strictEqual(matchingAirports.length, 2)
  })

  it('filters not case-sensitive', async function () {
    const matchingAirports = await requestSuggestions('zzz')
    assert.strictEqual(matchingAirports.length, 1)
  })

  it('returns maximum 6 suggestions', async function () {
    const matchingAirports = await requestSuggestions('A')
    assert.strictEqual(matchingAirports.length, 6)
  })
})

async function requestSuggestions (searchText) {
  const url = '/airports'
  const body = { search: searchText }

  const response = await fetch.post(url, body)
  return (await response.json()).airports
}
