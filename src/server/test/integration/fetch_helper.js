'use strict'

import fetch from 'isomorphic-fetch'

const URL = 'http://localhost:3030'

export default {
  post: function (relativeUrl, body) {
    return fetch(URL + relativeUrl, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
  }
}
