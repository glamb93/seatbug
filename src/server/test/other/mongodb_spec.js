'use strict'

/* global beforeEach, describe, it */

import assert from 'assert'

import Mongodb from '~/server/db/mongodb2.4'

describe('MongoDB', function () {
  const mongodb = Mongodb('mongodb://127.0.0.1/test')

  describe('Collection', function () {
    const INGREDIENT_1 = { name: 'Flour' }
    const INGREDIENT_2 = { name: 'Sugar' }

    const collection = mongodb.collection('ingredients')

    beforeEach(async function () {
      await collection.clear()
    })

    describe('When empty collection', async function () {
      describe('insertMany()', function () {
        it('does not side-effect the argument', async function () {
          const docsToAdd = [
            { ...INGREDIENT_1 },
            { ...INGREDIENT_2 }
          ]
          await collection.insertMany(docsToAdd)

          assert.deepStrictEqual(docsToAdd[0], INGREDIENT_1, 'Ingredient 1 was modified')
          assert.deepStrictEqual(docsToAdd[1], INGREDIENT_2, 'Ingredient 2 was modified')
        })

        it('adds new documents to db', async function () {
          const docsToAdd = [
            INGREDIENT_1,
            INGREDIENT_2
          ]
          let initialCount = await collection.count()
          await collection.insertMany(docsToAdd)

          let finalCount = await collection.count()
          assert.equal(finalCount, initialCount + docsToAdd.length)
        })

        it('returns the inserted document', async function () {
          const docsToAdd = [
            INGREDIENT_1,
            INGREDIENT_2
          ]
          let actual = await collection.insertMany(docsToAdd)

          for (let i = 0; i < actual.length; i++) {
            assert.deepStrictEqual(actual[i].name, docsToAdd[i].name, 'Ingredient 1 was modified')
            assert.ok(actual[i].id)
          }
        })
      })

      describe('insert()', function () {
        it('does not side-effect the argument', async function () {
          let initialIngredient = { ...INGREDIENT_1 }
          let testingArg = { ...initialIngredient }
          await collection.insert(testingArg)
          assert.deepEqual(testingArg, initialIngredient)
        })

        it('adds a new document to db', async function () {
          let initialCount = await collection.count()
          await collection.insert(INGREDIENT_1)

          let finalCount = await collection.count()
          assert.equal(finalCount, initialCount + 1)
        })

        it('returns the inserted document', async function () {
          let actual = await collection.insert(INGREDIENT_1)
          assert.deepEqual(actual.name, INGREDIENT_1.name)
          assert(!!actual.id)
        })
      })
    })

    describe('When collection with documents in it', async function () {
      const aValidUnexistingId = 'iiiiiiiiiiii'
      let ingredientWithId

      beforeEach(async function () {
        ingredientWithId = await collection.insert(INGREDIENT_2)
      })

      describe('clear()', function () {
        it('clears the collection', async function () {
          await collection.clear()
          assert.equal(await collection.count(), 0)
        })
      })

      describe('findAll()', function () {
        it('finds many documents by filter object', async function () {
          let result = await collection.findAll({ name: INGREDIENT_2.name })
          assert.deepEqual(result, [ ingredientWithId ])
        })
      })

      describe('get(id)', function () {
        it('gets a document by id', async function () {
          let result = await collection.get(ingredientWithId.id)
          assert.deepStrictEqual(result, ingredientWithId)
        })

        it('returns null if the id does not exist', async function () {
          let result = await collection.get(aValidUnexistingId)
          assert.strictEqual(result, null, 'Returned something else than null.')
        })
      })

      describe('remove(id)', function () {
        it('removes the ingredient with the given id', async function () {
          const countBefore = await collection.count()

          await collection.remove(ingredientWithId.id)

          const countAfter = await collection.count()
          assert.strictEqual(countAfter, countBefore - 1, 'The ingredient has not been removed')
        })
      })
    })
  })
})
