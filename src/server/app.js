'use strict'

import Server from '~/server/server.js'
import populateDatabase from '~/server/data/populate_database'
import Mongodb from '~/server/db/mongodb.js'

const IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

const PROD_SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 3030
const DEV_SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 3031
const END_TO_END_SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 3032

const MONGODB_URL = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://127.0.0.1:27017/'

const PROD_DB_NAME = process.env.OPENSHIFT_APP_NAME || 'seatbug'
const DEV_DB_NAME = 'seatbug-dev'
const END_TO_END_DB_NAME = 'seatbug-end-to-end'

export Mongodb from './db/mongodb.js'

export default function App (db) {
  const ENV = determineEnvironment(process.env.NODE_ENV)

  if (!db) {
    db = Mongodb(ENV.dbConnectionUrl)
  }
  populateDatabase(db, ENV.env === 'end-to-end')

  let server = Server(IP_ADDRESS, ENV.serverPort, db, 'public/', ENV.env !== 'production')
  server.start()

  if (ENV.env !== 'end-to-end') {
    console.info('\n\nServer listening on port ' + ENV.serverPort)
  }

  function stop () {
    if (server) {
      server.stop()
    } else {
      throw new Error('Server not started yet')
    }
  }

  return {stop}
}

function determineEnvironment (env) {
  switch (env) {
    case 'dev':
      return {
        env: 'dev',
        serverPort: DEV_SERVER_PORT,
        dbConnectionUrl: MONGODB_URL + DEV_DB_NAME
      }
    case 'end':
      return {
        env: 'end-to-end',
        serverPort: END_TO_END_SERVER_PORT,
        dbConnectionUrl: MONGODB_URL + END_TO_END_DB_NAME
      }
    default:
      return {
        env: 'production',
        serverPort: PROD_SERVER_PORT,
        dbConnectionUrl: MONGODB_URL + PROD_DB_NAME
      }
  }
}
