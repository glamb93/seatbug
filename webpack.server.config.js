'use strict'

var path = require('path')

const webpack = require('webpack')
const fs = require('fs')

let nodeModules = {}
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1
  })
  .forEach(function (mod) {
    nodeModules[mod] = 'commonjs ' + mod
  })

module.exports = {
  entry: [
    './src/server/init.js'
  ],
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
    modulesDirectories: [
      'app', 'node_modules'
    ]
  },
  externals: nodeModules,
  output: {
    path: path.join(__dirname, '/server_build'),
    publicPath: '/',
    filename: process.env.NODE_ENV === 'production' ? 'server.js' : 'server_dev.js'
  },
  target: 'node',
  plugins: [
    new webpack.BannerPlugin(
      'require("babel-polyfill");',
      { raw: true, entryOnly: false }
    )
  ]
}
